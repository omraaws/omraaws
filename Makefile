DEPS = base audiveris
DOCKER = docker

all: build

deps: $(DEPS)

base:
	$(MAKE) -C base build

audiveris: base
	$(MAKE) -C audiveris build

build: deps
	$(DOCKER) build -t omraaws/webservice:latest .

rmimage:
	$(DOCKER) rmi omraaws/webservice || true

run: build
	$(DOCKER) run --rm -it omraaws/webservice:latest

clean: rmimage

clean.base:
	$(MAKE) -C base rmimage

clean.audiveris:
	$(MAKE) -C audiveris rmimage

clean.recursive: clean clean.audiveris clean.base

.PHONY: all deps $(DEPS) build rmimage run \
	clean clean.base clean.audiveris clean.recursive
